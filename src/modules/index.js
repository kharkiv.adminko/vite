import router from "../router";
import { defineStore } from "pinia";

const register = (name, module) => {
  if (module.store) {
    defineStore(name, module.store);
  }

  if (module.router) {
    module.router(router);
  }
};

export const registerModules = (modules) => {
  Object.keys(modules).forEach((moduleKey) => {
    const module = modules[moduleKey];
    register(moduleKey, module);
  });
};
