import { defineStore } from "pinia";

import actions from "./actions";
import getters from "./getters";

export const useUserStore = defineStore("user", {
  state: () => ({
    users: [
      { id: 1234, name: " user 1234" },
      { id: 5678, name: " user 5678" },
    ],
    page: 1,
  }),
  getters,
  actions,
});
