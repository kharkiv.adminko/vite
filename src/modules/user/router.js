import Index from "@/modules/user/components/Index.vue";
import Show from "@/modules/user/components/Show.vue";
import Module from "@/modules/user/Module.vue";

const routes = {
  path: "/users",
  component: Module,
  children: [
    { path: "", component: Index, name: "users.index" },
    { path: ":id", component: Show, name: "users.show" },
  ],
};

export default (router) => {
  router.addRoute(routes);
};
