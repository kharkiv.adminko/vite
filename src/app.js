import { createApp } from "vue";
import store from "./store";
import router from "./router";
import App from "./App.vue";
import { registerModules } from "./modules";
import User from "./modules/user";

registerModules({
  User,
});

const app = createApp(App);

app.use(store);
app.use(router);
app.mount("#app");
