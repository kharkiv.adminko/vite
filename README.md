# Vite + Vue + VueRouter + Pinia

## Requirements

- Docker
- Docker-compose

## Installation

- clone repo
- copy `.env.example` as `.env`
- run `docker-compose up -d`
- pass into container `docker-compose exec vite bash`
- install dependencies `yarn install`
- start server with `yarn dev`
